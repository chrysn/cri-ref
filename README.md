cri-ref: Rust implementation of CRI references
==============================================

This crate implements according to a work-in-progress [Internet Draft for constrained-device URIs]: CRIs.
These are intended to replace URIs in embedded devices that can not afford heavy string handling.

It both provides a concrete implementation (in the `native_cbor` module)
and a trait abstraction (in the `accessor` and `traits` modules).
Both of them are still in flux, and subject to experimentation.
In particular,
an iterating trait interface is being explored in `iterator`
(which would only allow access to components only in the sequence you need it,
but without any pre-parsing cost).
One to-be-explored concrete type is a slice of encoded CBOR data
(which on the type level might be distinguished between known-good and arbitrary-data).
That would be a bit more costly to access compared to the current heap based version,
but could do without heap allocation,
and accessing it through the iterating interface should be as fast as accessing preparsed data.

Some experimentation with the traits so far has shown
that the accessor interface might be good enough:
When done carefully on memory the compiler can know to be constant,
the duplicate code of seeking to the host, the path and the query in sequence
can be removed by duplicate code detection.

Additional experimentation is ongoing in linking CRIs with CoAP messages:
A CRI might be turned into CoAP request's options to indicate a path,
the response might alter the request URI (eg. when evaluating a multicast response),
and data inside the response might be resolved relative to the response's base URI/CRI
to be persisted or made into a new CoAP request
-- all without needless allocations or re-encodings.

[Internet Draft for constrained-device URIs]: https://datatracker.ietf.org/doc/draft-ietf-core-href/

Using this as a library
-----------------------

So far this is heavily discouraged due to the unstable APIs,
and due to not being `no_std` yet <!-- see also Cargo.toml when this changes -->.

### CRI features

Once this is usable as a library,
it will likely stay a relatively complete implementation -- supporting percent encoded text and all the other `.feature`s that make things complicated.
These features could be mapped to Rust features selectable by dependent crates.
It is envisioned that embedded users will not select features they do not need,
and that considerable complexity will automatically be left out when certain enum variants are not available any more.

Testing
-------

The test suite provided in [the draft's repository] is evaluated in `cargo test`.

[in the draft's repository]: https://github.com/core-wg/href/tree/main/tests
