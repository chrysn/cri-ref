use coap_message::MessageOption;

/// The CRI implied by the Location options of a CoAP message
pub struct Location<M>(pub M) where M: coap_message::ReadableMessage;

impl<M> crate::accessor::CriRef for Location<M>
where
    M: coap_message::ReadableMessage
{
    type Scheme = !;
    type Host = !;

    type PathIter<'a> = impl Iterator<Item=&'a str>;
    type QueryIter<'a> = impl Iterator<Item=&'a str>;

    fn path(&self) -> Self::PathIter<'_> {
        self.0.options()
            .filter(|o| o.number() == coap_numbers::option::LOCATION_PATH)
            // This lifetime issue here is symtomatic for the signature: The trait is demanding a
            // &'a str, which some CoAP implementations may not manage to provide (eg. because
            // their messages sit in a scatter-gather buffer, and their iterators assemble the [u8]
            // to point in .value() on demand)
            .filter_map(|o| core::str::from_utf8(o.value()).ok())
    }

    fn fragment(&self) -> Option<&str> {
        None
    }
}
