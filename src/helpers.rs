//! Generic implementations of traits of this crate

use crate::traits;

// being generic this can also be used in non-heap types, but we don't have any right now
#[derive(Debug)]
#[non_exhaustive]
pub enum Scheme<T: AsRef<str>> {
    // Could just as well be a Numeric(i16)
    Coap,
    CoapS,
    Http,
    HttpS,
    Textual(T),
}

impl<T: AsRef<str>> traits::Scheme for Scheme<T> {
    fn to_cri_id(&self) -> Option<i16> {
        Some(match self {
            Scheme::Coap => -1,
            Scheme::CoapS => -2,
            Scheme::Http => -3,
            Scheme::HttpS => -4,
            _ => return None,
        })
    }

    fn to_text_scheme(&self) -> &str {
        match self {
            Scheme::Textual(t) => t.as_ref(),
            Scheme::Coap => "coap",
            Scheme::CoapS => "coaps",
            Scheme::Http => "http",
            Scheme::HttpS => "https",
        }
    }
}

