#![feature(type_alias_impl_trait)]
#![feature(generic_associated_types)]
#![feature(never_type)] // used for the ! impls which are used by coap but also provided for others

mod characterclasses;
mod helpers;

pub mod traits;
// Several modules may in future provide CRI-ish traits; accessor-based, sequence-of-options or
// type-based
pub mod accessor;
// Incomplete / testing
// mod iterator;

pub mod native_cbor;

#[cfg(feature = "coap")]
pub mod coap;

#[cfg(test)]
mod vectors;
