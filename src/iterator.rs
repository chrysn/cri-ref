//! Traits to access a CRI (or CRI reference) as an iteration over components

#[derive(Debug)]
enum Component<'a> {
    // following the options of -02
    Scheme(&'a str),
    HostName(&'a str), HostIp(&'a [u8]),
    Port(u16),
    // but diverging into  the discard semantics
    Discard(crate::accessor::Discard),
    Path(&'a str),
    Query(&'a str),
    Fragment(&'a str),
}

trait CursorPosition {
    fn get(&self) -> Component<'_>;
}

trait CriRef {
    // Can't go directly to a Component because some iterators produce them in a short-lived
    // fashion and may not have the capacity to hand out the pieces as memory slices that live as
    // long as the iterator.
    type Iter: Iterator<Item=Self::CursorPosition>;
    type CursorPosition: CursorPosition;

    fn components(&self) -> Self::Iter;
}


/// Getting from Accessor pattern to Iterator pattern
pub struct IteratorFromAccessor<C: crate::accessor::CriRef>(C);

impl<C: crate::accessor::CriRef> CriRef for IteratorFromAccessor<C> {
    type Iter = ComponentProducer<C>;
    type CursorPosition = impl CursorPosition;

    fn components(&self) -> Self::Iter {
    }
}

enum ComponentProducer<C: crate::accessor::CriRef> {
    Initial,
    ProducingPath(C::PathIter),
}
